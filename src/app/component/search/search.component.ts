import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms'
import { Router } from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  searchForm: any
  error = false
  login = ''
  constructor(
    private fb: FormBuilder,
    private router: Router    
    ) { }

  ngOnInit(): void {
    this.searchForm = FormGroup
    this.searchForm = this.fb.group({
      login: ['', [Validators.required, Validators.pattern(/^\S*$/),]]
    })
  }
  submit(){
    this.error = false
    if (this.searchForm.valid) {
      this.login = this.searchForm.value.login
      this.searchForm.reset();
      this.router.navigate([`/results/${this.login}`])  
    }
    else{
      this.error = true
    }
  }
  reset(){
    this.searchForm.reset();
  }
}
