import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SearchComponent } from './search/search.component'
import { ResultsComponent } from './results/results.component'

const routes: Routes = [
  {path:'', component: SearchComponent},
  {path:'results/:login',component: ResultsComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainlayoutRoutingModule { }
