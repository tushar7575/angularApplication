import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainlayoutRoutingModule } from './component-routing.module';
import { SearchComponent } from './search/search.component';
import { ResultsComponent } from './results/results.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ApiService } from '../controller/controller.service';
import { HttpClientModule } from '@angular/common/http';
import { DataTablesModule } from 'angular-datatables';


@NgModule({
  declarations: [SearchComponent, ResultsComponent],
  imports: [
    CommonModule,
    MainlayoutRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    DataTablesModule
  ],
  providers: [ApiService],
})
export class MainlayoutModule { }
