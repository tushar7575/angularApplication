import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from 'src/app/controller/controller.service';
import { ActivatedRoute, Router } from '@angular/router';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css']
})
export class ResultsComponent implements OnInit {
  login = ''
  resultData
  tableParam: any = {};
  dtTrigger: Subject<any> = new Subject();
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  constructor(private  apiService:ApiService,private route: ActivatedRoute, public router: Router,) { }

  ngOnInit(): void {
    this.login = this.route.snapshot.params['login']
    console.log(this.login)
    this.tableParam = {
      pageLength: 9,
      pagingType: 'full_numbers',
      processing: true,
      responsive: true,
      'stateSave': true,
      'stateSaveCallback': function(settings,data) {
        localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
      },
      'stateLoadCallback': function(settings) {
        return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance ))
      },
      "columnDefs": [
        { "sortable": true, "targets": [1] },
        { "responsivePriority": 1, "targets": [0, 2] },
      ]
    };
    this.apiService.getAPI(`earch/users?q=${this.login}`).subscribe((data) => {
      console.log(data) 
      this.resultData = data['items']
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.destroy()
        this.dtTrigger.next()
      })
    })
  }
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }
  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }
}
