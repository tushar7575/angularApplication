import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  baseUrl = 'https://api.github.com/search/users?q=foo%20in:login'

  constructor(private http: HttpClient) { }
  
  public getAPI(url:any) {
    return this.http.get(`${this.baseUrl}/${url}`)

  }
}
